package com.innovect.weatherforcasting.service;

import org.springframework.stereotype.Service;

import com.innovect.weatherforcasting.model.Forecast;

/**
 * 
 * @author Kajal
 *
 */
@Service
public interface WeatherForcastService {

    Forecast getHourlyForcast(String countryCode, String zipCode);

}
