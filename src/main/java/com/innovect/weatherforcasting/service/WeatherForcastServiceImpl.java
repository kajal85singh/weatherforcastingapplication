package com.innovect.weatherforcasting.service;

import com.innovect.weatherforcasting.common.WeatherRest;
import com.innovect.weatherforcasting.model.Forecast;
import com.innovect.weatherforcasting.model.Weather;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 
 * @author Kajal
 *
 */
@Service
public class WeatherForcastServiceImpl implements WeatherForcastService{

	@Autowired
	private WeatherRest weatherRest;

    @Override
	public Forecast getHourlyForcast(String countryCode, String zipCode) {
		Forecast forecast = weatherRest.getHourlyForecast(countryCode, zipCode);
		LocalDateTime localDateTime = LocalDateTime.now(ZoneId.of(forecast.getTimezone()));
		List<Weather> weatherList = forecast.getWeatherList().stream()
				.filter(weather -> weather.getLocalDateTime().toLocalDate().isAfter(localDateTime.toLocalDate()))
				.collect(Collectors.toList());
		Weather weather = null;
		forecast.setWeatherList(weatherList);
		weather = Collections.min(weatherList, Comparator.comparing(Weather::getTemperature));
		forecast.setWeatherList(weatherList);
		forecast.setCoolestHourOfDay(weather.getLocalDateTime());
		return forecast;
	}
}
