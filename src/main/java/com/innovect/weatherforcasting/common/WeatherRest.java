package com.innovect.weatherforcasting.common;

import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.innovect.weatherforcasting.model.Forecast;

import java.net.URISyntaxException;

/**
 * 
 * @author Kajal
 *
 */
@Component
public class WeatherRest {

 private static final Logger log = LoggerFactory.getLogger(WeatherRest.class);

	@Value("${weatherbit.api.key}")
	private String apiKey;

	@Value("${weatherbit.api.url}")
	private String url;

	@Value("${default.lang}")
	private String lang;

	@Value("${daily.hours}")
	private String duration;

	@Autowired
	private RestTemplate restTemplate;

	public Forecast getHourlyForecast(String countryCode, String zipCode) {
		log.info("WeatherUtill.getHourlyForecast()");
		Forecast forecast = new Forecast();
		try {
			forecast = restTemplate.getForObject(urlBuilder(countryCode, zipCode), Forecast.class);
		}catch (Exception e){
			log.error(e.getMessage());
		}
		return forecast;
	}

	private String urlBuilder(String countryCode, String zipCode) {
		String urlString = null;
		try {
			URIBuilder b = new URIBuilder(url);
			b.addParameter("postal_code", zipCode);
			b.addParameter("country", countryCode);
			b.addParameter("units", "S");
			b.addParameter("lang",lang);
			b.addParameter("duration",duration);
			b.addParameter("key", apiKey);
			urlString = b.build().toString();
		} catch (URISyntaxException e) {
			log.error(e.getMessage());
		}
		return urlString;
	}

}
