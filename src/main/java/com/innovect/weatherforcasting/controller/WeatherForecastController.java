package com.innovect.weatherforcasting.controller;

import com.innovect.weatherforcasting.model.Forecast;
import com.innovect.weatherforcasting.service.WeatherForcastService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @author Kajal
 *
 */
@RestController
public class WeatherForecastController {

private static final Logger log = LoggerFactory.getLogger(WeatherForecastController.class);

	@Autowired
	private WeatherForcastService weatherForcastService;

	@GetMapping(value = "/weather/{countryCode}/{zip}")
	public Forecast getWeatherForecast(@PathVariable("countryCode") String countryCode,
			@PathVariable("zip") String zipCode) {
		log.info("WeatherForecastController.getWeatherForecast()");
		
		return weatherForcastService.getHourlyForcast(countryCode, zipCode);
	}
}
