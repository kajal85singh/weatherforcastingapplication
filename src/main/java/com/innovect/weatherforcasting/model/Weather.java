package com.innovect.weatherforcasting.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;

/**
 * 
 * @author Kajal
 *
 */
public class Weather {
	private double temperature;

	private LocalDateTime localDateTime;

	public Weather() {
	}

	@JsonCreator
	public Weather(@JsonProperty("temp") double temperature,
			@JsonProperty("timestamp_local") LocalDateTime localDateTime) {
		this.temperature = temperature;

		this.localDateTime = localDateTime;
	}

	public double getTemperature() {
		return temperature;
	}

	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}

	public LocalDateTime getLocalDateTime() {
		return localDateTime;
	}

	public void setLocalDateTime(LocalDateTime localDateTime) {
		this.localDateTime = localDateTime;
	}

}
