package com.innovect.weatherforcasting.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import com.innovect.weatherforcasting.common.WeatherRest;
import com.innovect.weatherforcasting.model.Forecast;
import com.innovect.weatherforcasting.model.Weather;

@RunWith(MockitoJUnitRunner.class)
public class WeatherForcastServiceImplTest {

    @InjectMocks
    WeatherForcastServiceImpl weatherForcastServiceImpl;


    @Mock
    private WeatherRest weatherRest ;


    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetHourlyForcast(){
        List<Weather> weathers = new ArrayList<>();
        weathers.add(new Weather(25.0, LocalDateTime.now().plusDays(1)));
        weathers.add(new Weather(20.0, LocalDateTime.now().plusDays(2)));
        Forecast forecast = new Forecast();
        forecast.setWeatherList(weathers);
        forecast.setCountryCode("US");
        forecast.setStateCode("AK");
        forecast.setTimezone("America/Anchorage");
        forecast.setCoolestHourOfDay(LocalDateTime.now().plusDays(1));
        when(weatherRest.getHourlyForecast("USA","99501")).thenReturn(forecast);
        Forecast result = weatherForcastServiceImpl.getHourlyForcast("USA", "99501");
        assertEquals(2, result.getWeatherList().size());
        assertEquals(LocalDateTime.now().plusDays(2).getDayOfMonth(),result.getCoolestHourOfDay().getDayOfMonth() );

    }
}
